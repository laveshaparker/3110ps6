open Definitions

type collisions = {
  red_kills : bullet list;
  red_grazes : bullet list;
  red_powers : power list;
  blue_kills : bullet list;
  blue_grazes : bullet list;
  blue_powers : power list;
  u_kills : (ufo * bullet list) list
}

type team = {
  mutable lives : int;
  mutable bombs : int;
  mutable score : int;
  mutable power : int;
  mutable charge : int;
  mutable player : player_char;
  mutable movesQueue : (direction * direction) list;
  mutable invincible : (bool * int)
}

type game = {
  red_team : team;
  blue_team : team;
  mutable ufos : ufo list;
  mutable bullets : bullet list;
  mutable powers : power list;
  mutable current_time : float;
  mutable collisions : collisions;
  mutable remaining_bomb_duration : color * float;
}