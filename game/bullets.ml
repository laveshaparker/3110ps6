open MyDefinitions
open Definitions
open MyDefinitions
open Constants
open Util

let createBullet game col bull_type pos vel radius accel =
    let id = next_available_id () in
    (*updateTeamBullets game col id;*)
    let bullet = {
    b_type = bull_type;
    b_id = id;
    b_pos = pos;
    b_vel = vel;
    b_accel = accel;
    b_radius = radius;
    b_color = col
    } in Netgraphics.add_update (AddBullet(id, col, bull_type, pos));
    bullet

let deductCharge game chargeAmount col =
  match col with
  |Red -> game.red_team.charge <- game.red_team.charge - chargeAmount
  |Blue -> game.blue_team.charge <- game.blue_team.charge - chargeAmount

let hasCharge team chargeAmount =
  team.charge > chargeAmount

let addBulletToGame game bullet =
  game.bullets <- (bullet::game.bullets); game

let addBulletsToGame game bulletList =
  game.bullets <- (bulletList@game.bullets); game  

let playerShot game col b_type player_pos target accel =
  let team = match col with
  | Red -> game.red_team
  | Blue -> game.blue_team in
  match b_type with
  | Bubble -> if not (hasCharge team cBUBBLE_COST) then game else 
  let unit_vector = unit_v (subt_v target player_pos) in
  let vel = scale (float_of_int cBUBBLE_SPEED) unit_vector in
    let newBullet = createBullet game col b_type player_pos vel 
    cBUBBLE_RADIUS accel in
    deductCharge game cBUBBLE_COST col;
    addBulletToGame game newBullet
  | Trail -> if not (hasCharge team cTRAIL_COST) then game else 
    let unit_vector = unit_v (subt_v target player_pos) in
    let vel = scale (float_of_int cTRAIL_SPEED_STEP) unit_vector in
    let rec createTrail acc bulletList vel speed =
      match acc == cTRAIL_NUM with
      |true -> bulletList
      |false -> 
      let newBullet = createBullet game col b_type player_pos 
                      (scale speed vel) cTRAIL_RADIUS accel in
      createTrail (acc + 1) (newBullet::bulletList) vel 
                  (speed +. (float_of_int cTRAIL_SPEED_STEP))
    in 
    let createTrails () =
      let rad_rot = rad_to_deg (float_of_int cTRAIL_ANGLE) in
      let step = (float_of_int cTRAIL_SPEED_STEP) in
      (createTrail 0 [] (rotate vel (-.rad_rot)) step)@
      (createTrail 0 [] vel step)@
      (createTrail 0 [] (rotate vel rad_rot) step) in
      deductCharge game cTRAIL_COST col;
      addBulletsToGame game (createTrails ())

  | Spread -> if not (hasCharge team cSPREAD_COST) then game else 
    let unit_vector = (unit_v (subt_v target player_pos)) in
    let vel = scale (float_of_int cSPREAD_SPEED) unit_vector in
    let rotation = (2.*.pi)/.(float_of_int cSPREAD_NUM) in
    let rec createSpread acc bulletList vel =
      match acc == cSPREAD_NUM with
        |true -> bulletList
        |false -> 
        let newBullet = createBullet game col b_type player_pos vel 
                        cSPREAD_RADIUS accel in
        createSpread (acc + 1) (newBullet::bulletList) (rotate vel rotation)
      in
      deductCharge game cSPREAD_COST col;
      addBulletsToGame game (createSpread 0 [] vel)
  | Power -> failwith "player shouldn't shoot this"
  

let updateBulletPosition bullet =
  let (x, y) = bullet.b_pos in
  let (velX, velY) = bullet.b_vel in
  let (accelX, accelY) = bullet.b_accel in
  let newPos = ((x +. velX), (y +. velY)) in
  let newBullet : bullet =  {
    b_type = bullet.b_type;
    b_id = bullet.b_id;
    b_pos = newPos;
    b_vel = ((velX +. accelX), (velY +. accelY));
    b_accel = bullet.b_accel;
    b_radius = bullet.b_radius;
    b_color = bullet.b_color
  } in Netgraphics.add_update (MoveBullet(bullet.b_id, newPos));
  newBullet

let removeBulletsOutOfBounds (b_list : bullet list) : bullet list =
  List.fold_left (fun bulletList nextBullet -> if (in_bounds nextBullet.b_pos) 
    then (nextBullet::bulletList) 
    else (Netgraphics.add_update (DeleteBullet(nextBullet.b_id)); bulletList)) 
  [] b_list

let removeBulletsFromGameData (game : game) (hit_list : bullet list) : unit =
  let fold_helper (acc : bullet list) (b : bullet) : bullet list =
    match (List.exists (fun u -> u.b_id = b.b_id) hit_list) with
    | true ->  Netgraphics.add_update (DeleteBullet(b.b_id)); acc
    | false -> b::acc
  in
  game.bullets <- List.fold_left fold_helper [] game.bullets

let removeBulletsHittingUfos (game : game) : unit =
  let u_hit_list = 
    let fold_helper (acc : bullet list) (u_b_list : (ufo * bullet list)) =
      let (_, b_list) = u_b_list in
      b_list@acc
    in
    List.fold_left fold_helper [] game.collisions.u_kills
  in
  removeBulletsFromGameData game u_hit_list
  
let removeGrazingBullets (game : game) : unit =
  let (col, dur) = game.remaining_bomb_duration in
    match dur > 0. with
    | true -> ();
    | false -> 
      begin
        let fold_helper (acc : unit) (b : bullet) : unit =
          Netgraphics.add_update (DeleteBullet(b.b_id))
        in
        List.fold_left fold_helper () game.collisions.red_grazes;
        List.fold_left fold_helper () game.collisions.blue_grazes;
        let rem = (game.collisions.red_grazes@game.collisions.blue_grazes) in
        removeBulletsFromGameData game rem
      end

let updateBullets (bullets : bullet list) : bullet list =
  let b_list = (List.map updateBulletPosition bullets) in
  removeBulletsOutOfBounds b_list

