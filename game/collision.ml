open MyDefinitions
open Definitions
open MyDefinitions
open Constants

(* Returns a list of all of the bullets in the game that belong to the
   opposing team *)
let find_other_teams_bullets (team : team) (b_list : bullet list) =
  let fold_helper (acc : bullet list) (b : bullet) : bullet list =
    match b.b_color = team.player.p_color with
    | true -> acc
    | false -> b::acc
  in
  List.fold_left fold_helper [] b_list

(* (player/ufo position, player/ufo radius, bullet, optional additional 
    bullet radius) returns true if there is a collision that results in a 
    hit, false otherwise. *)
let is_hit (pos : position) (radius : int) (rad2 : int) (b : bullet) : bool =
  let (px, py) = pos in
  let (bx, by) = b.b_pos in
  let kill_rad = rad2 + cHITBOX_RADIUS in
  let distance = sqrt(((px-.bx)**2.) +. ((py-.by)**2.)) in
  let sum_radii = (float_of_int radius) +. (float_of_int kill_rad) in
  distance<sum_radii

(* player position, player radius, bullet, returns true if
   there is a collision that result in a graze, false otherwise. *)
let is_graze (pos : position) (radius : int) (b : bullet) : bool =
  is_hit pos radius cGRAZE_RADIUS b

(* Returns a list of bullets that have collided with and killed
   a team's player. *)
(* player, invincible?, other team's bullets, result *)
let p_killHits (player : player_char) (b : bool * int) (b_list : bullet list)=
  let fold_helper (acc : bullet list) (b : bullet) : bullet list =
    match is_hit player.p_pos player.p_radius 0 b with
    | true -> b::acc
    | false -> acc
  in
  List.fold_left fold_helper [] b_list

(* Returns a list of bullets that have grazed (but not killed) a team's *)
(* player player, opponent's bullets that killed this player, opponent's *)
(* bullets, result *)
let p_grazeHits player k_list b_list =
  let fold_helper1 (acc : bullet list) (b : bullet) : bullet list =
    match is_hit player.p_pos player.p_radius 0 b with
    | true -> b::acc
    | false -> acc
  in
  let possible = List.fold_left fold_helper1 [] b_list in
  let fold_helper2 (acc : bullet list) (b : bullet) : bullet list =
    match List.exists (fun x -> x.b_id = b.b_id) k_list with
    | true -> acc
    | false -> b::acc
  in
  List.fold_left fold_helper2 [] possible

(* Returns a list of powers that have collided with a team's player. *)
let p_powerHits (player : player_char)  (p_list : power list) : power list =
  let fold_helper (acc : power list) (p : power) : power list =
    match is_hit player.p_pos player.p_radius 0 p with
    | true -> p::acc
    | false -> acc
  in
  List.fold_left fold_helper [] p_list

(* Returns a list of bullets that have collided with any ufo. *)
let u_killHits (u_list : ufo list)  (b_list : bullet list) =
  let inner_fold_helper (ufo : ufo ) : bullet list =
    let fold_helper (acc : bullet list) (b : bullet) : bullet list =
      match is_hit ufo.u_pos ufo.u_radius 0 b with
      | true -> b::acc
      | false -> acc
    in
    List.fold_left fold_helper [] b_list
  in
  let outer_fold_helper (acc : (ufo * bullet list) list) (u : ufo) =
    let (hits : bullet list) = inner_fold_helper u in
    match (List.length hits) > 0 with
    | true -> (u, hits)::acc
    | false -> acc
  in
  List.fold_left outer_fold_helper [] u_list

let all_collisions (game : game) : collisions =
  let blueB = find_other_teams_bullets game.red_team game.bullets in
  let redB = find_other_teams_bullets game.blue_team game.bullets in
  let rk = p_killHits game.red_team.player game.red_team.invincible blueB in
  let bk = p_killHits game.blue_team.player game.blue_team.invincible redB in
  let return = {
  red_kills = rk;
  red_grazes = p_grazeHits game.red_team.player rk blueB;
  red_powers = p_powerHits game.red_team.player game.powers;
  blue_kills = bk;
  blue_grazes = p_grazeHits game.blue_team.player bk redB;
  blue_powers = p_powerHits game.blue_team.player game.powers;
  u_kills = u_killHits game.ufos game.bullets
  } in
  return