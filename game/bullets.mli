open Definitions
open MyDefinitions

val createBullet : game -> color -> bullet_type -> position -> velocity -> int -> acceleration -> bullet

val playerShot : game -> color -> bullet_type -> position -> position -> acceleration -> game

val updateBulletPosition : bullet -> bullet

val updateBullets : bullet list -> bullet list

val removeBulletsHittingUfos : game -> unit

val removeGrazingBullets : game -> unit 

