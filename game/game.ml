open Definitions
open MyDefinitions
open Constants
open Util
open Players
open Bullets
open Ufo

(***************************Data stuctures**********************************)

(* Beginning of helper functions *)

(* This takes a list of updates and adds each update to the gui's 
   list of updates *)
let rec sendBatchUpdates (updateList : update list) : unit =
  match updateList with
  | [] -> ()
  | h::tl -> Netgraphics.add_update h; sendBatchUpdates tl

let removeAllBullets game =
  let map_helper (bullet : bullet) = DeleteBullet(bullet.b_id) in
  let updateList = List.map map_helper game.bullets in
  sendBatchUpdates updateList;
  game.bullets <- []

let updateCharge game =
  let r_sum = game.red_team.charge + cCHARGE_RATE in
  if (r_sum <= cCHARGE_MAX) then
  game.red_team.charge <- game.red_team.charge + cCHARGE_RATE
  else game.red_team.charge <- cCHARGE_MAX;
  let b_sum = game.blue_team.charge + cCHARGE_RATE in
  if (b_sum <= cCHARGE_MAX) then
  game.blue_team.charge <- game.blue_team.charge + cCHARGE_RATE
  else game.blue_team.charge <- cCHARGE_MAX

let handle_bomb (game : game) (col : color) : unit =
  let team = (
    match col with
    | Red -> game.red_team
    | Blue -> game.blue_team) in
    match team.bombs > 0 with
    | true -> 
      begin
      team.bombs <- max 0 (team.bombs - 1);
      Netgraphics.add_update (SetBombs(col, team.bombs));
      removeAllBullets game;
      Netgraphics.add_update(UseBomb(col)); 
      team.invincible <- (true, cBOMB_DURATION);
      game.remaining_bomb_duration <- (col, float_of_int cBOMB_DURATION)
      end
    | false -> ()

let team_data (t : team) : team_data =
  let data = (t.lives, t.bombs, t.score, t.power, t.charge, t.player) in
  data

(* End of helper functions *)

let init_game () : MyDefinitions.game =
  let x = 1./.8. *. (float_of_int cBOARD_WIDTH) in
  let y = 1./.2. *. (float_of_int cBOARD_HEIGHT) in
  let r_pos = (x, y) in
  let r_id = next_available_id () in
  let red_player = {
  	p_id = r_id;
  	p_pos = r_pos;
  	p_focused = false;
  	p_radius = cGRAZE_RADIUS;
		p_color = Red
	} in
  let x' = 7./.8. *. (float_of_int cBOARD_WIDTH) in
  let y' = 1./.2. *. (float_of_int cBOARD_HEIGHT) in 
  let b_pos = (x', y') in
  let b_id = next_available_id () in
  	let blue_player = {
  		p_id = b_id;
  		p_pos = b_pos;
  		p_focused = false;
  		p_radius = cGRAZE_RADIUS;
  		p_color = Blue
  	} 
  in 
  	let red_team = {
  		lives = cINITIAL_LIVES;
  		bombs = cINITIAL_BOMBS;
  		score = 0;
  		power = 0;
  		charge = 0;
  		player = red_player;
      movesQueue = [];
      invincible = (false, 0)
  	} in 
  	let blue_team = {
  		lives = cINITIAL_LIVES;
  		bombs = cINITIAL_BOMBS;
  		score = 0;
  		power = 0;
  		charge = 0;
  		player = blue_player;
      movesQueue = [];
      invincible = (false, 0)
  	} in 
    let collisions' ={
      red_kills = [];
      red_grazes = [];
      red_powers = [];
      blue_kills = [];
      blue_grazes = [];
      blue_powers = [];
      u_kills = [];
    }in
  	let game = {
  		red_team = red_team;
  		blue_team = blue_team;
  		ufos = [];
  		bullets = [];
  		powers = [];
  		current_time = 0.;
      collisions = collisions';
      remaining_bomb_duration = (Red, 0.)
  	} in 
    let graphicsList = [(AddPlayer(b_id, Blue, b_pos)); 
      (AddPlayer(r_id, Red, r_pos)); 
      (SetLives(Red, cINITIAL_LIVES)); 
      (SetLives(Blue, cINITIAL_LIVES))] in
    sendBatchUpdates graphicsList;
  game
 
let handle_time (game : game) : (game * result) =
  game.bullets <- updateBullets game.bullets;
  game.current_time <- game.current_time +. cUPDATE_TIME;
  updatePlayers game;
  updateCharge game;
  updateUFOs game;
  updatePowers game;
  removeBulletsHittingUfos game;
  let (col,dur) = game.remaining_bomb_duration in
  game.remaining_bomb_duration <- (col,(max (dur-.cUPDATE_TIME) 0.));
  removeGrazingBullets game;
  match (game.red_team.lives > 0), (game.blue_team.lives>0) with
  | true, true -> 
    begin
      match (game.current_time < cTIME_LIMIT) with
      | true -> (game, Unfinished) 
      | false -> 
        begin
          match (game.red_team.score > game.blue_team.score) with
          |true -> (game, Winner(Red))
          |false -> if (game.red_team.score = game.blue_team.score) 
          then (game, Tie)
          else (game, Winner(Blue))
        end
    end
  | true, false -> (game, Winner(Red))
  | false, true -> (game, Winner(Blue))
  | false, false -> (game, Tie)

let handle_action game col act =
  match act with
  | Move(moveList) -> 
    (match col with
    |Red -> game.red_team.movesQueue <- moveList
    |Blue -> game.blue_team.movesQueue <- moveList
  );
    game
  | Shoot(bullet, position, accel) -> 
    let newAccel = match accel with (x, y) ->
    let newX =
    if (x > cACCEL_LIMIT) then 0. else x in
    let newY =
    if (y > cACCEL_LIMIT) then 0. else y in
    (newX, newY) in
    let startPos = getPlayerPosition game col in
    playerShot game col bullet startPos position newAccel
  | Focus(state) -> changeFocus game col state; game
  | Bomb -> handle_bomb game col; game


let get_data g =
  let redTeamData = team_data g.red_team in
  let blueTeamData = team_data g.blue_team in
  let game_data = (redTeamData, blueTeamData, g.ufos, g.bullets, g.powers) in
  game_data