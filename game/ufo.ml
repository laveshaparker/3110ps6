open Definitions
open MyDefinitions
open Constants
open Util
open Bullets

(* Creates a new ufo if it is the right time to do so. *)
let createUFO (g : game) : unit =
  (* Returns true if a ufo should be spawned now, false otherwise. *)
  let spawnUFO (g : game) : bool =
    let time = g.current_time in
    let numTimeSteps = int_of_float (time/.cUPDATE_TIME) in
    let denom = cUFO_SPAWN_INTERVAL in
    ((mod) numTimeSteps denom) = 0
  in
  (* Generates a random point for a newly spawned UFO to begin at. *)
  let initialPoint (u : unit) : position =
    let x =
      let lower = 0.25 *. (float_of_int cBOARD_WIDTH) in
      let upper = 0.75 *. (float_of_int cBOARD_WIDTH) in
      let interval = upper -. lower in
      lower +. (Random.float interval)
    in
    let y =
      match Random.int 2 with
      | 0 -> 0.
      | 1 -> (float_of_int cBOARD_HEIGHT)
      | _ -> failwith ("just to get rid of compile warning."^
        " Random.int is non-inclusive on upper bound")
    in
    (x,y)
  in
  (* Generates a random point for the ufo to head toward *)
  let randomPoint (u : unit) : position =
    let x =
      let lower = 0. in
      let upper = (float_of_int cBOARD_WIDTH) in
      let interval = upper -. lower in
      lower +. (Random.float interval)
    in
    let y =
      let lower = 0. in
      let upper = (float_of_int cBOARD_HEIGHT) in
      let interval = upper -. lower in
      lower +. (Random.float interval)
    in
    (x,y)
  in
  (* Given the start location of a ufo, generates a random velocity. *)
  let velocity (initial_position : position) : velocity =
    let (x,y) = initial_position in
    let (x',y') = randomPoint () in
    let dist = sqrt (((x-.x')**2.) +. ((y-.y')**2.)) in
    let new_x = (float_of_int cUFO_SPEED) *. ((x'-.x)/.dist) in
    let new_y = (float_of_int cUFO_SPEED) *. ((y'-.y)/.dist) in
    (new_x, new_y)
  in
	match spawnUFO g with
	| true -> 
	begin
		let initial_position = initialPoint () in
    let new_id = next_available_id () in
		let return = {
		u_id = new_id;
		u_pos = initial_position;
		u_vel = velocity initial_position;
		u_radius = cUFO_RADIUS;
		u_red_hits = 0;
		u_blue_hits = 0;
		}
	    in
    Netgraphics.add_update (AddUFO(new_id, initial_position));
		g.ufos <- return::g.ufos
	end
	| false -> ()

let createPowers (g : game) (u : ufo) (red_num : int) : unit = 
  let rec powerLocation (pos : position) : position =
    let (ux, uy) = pos in
    let random_x = (Random.float ((float_of_int cUFO_SCATTER_RADIUS)*.2.)) in
    let random_y = (Random.float ((float_of_int cUFO_SCATTER_RADIUS)*.2.)) in
    let px = ux +. random_x -. (float_of_int cUFO_SCATTER_RADIUS) in
    let py = uy +. random_y -. (float_of_int cUFO_SCATTER_RADIUS) in
    let distance = (sqrt((px-.ux)/.(py-.uy))) in
    match distance <= (float_of_int cUFO_SCATTER_RADIUS) with
    | true -> (px, py)
    | false -> powerLocation pos
  in
  let power_velocity initial_position player's_location =
    let (x,y) = initial_position in
    let (x',y') = player's_location in
    let dist = sqrt (((x-.x')**2.) +. ((y-.y')**2.)) in
    let new_x = (float_of_int cPOWER_SPEED) *. ((x'-.x)/.dist) in
    let new_y = (float_of_int cPOWER_SPEED) *. ((y'-.y)/.dist) in
    (new_x, new_y)
  in
  (* game -> ufo -> position -> int -> power list -> power list *)
  let rec createPowersRec g u player_loc amt acc  =
    match amt with
    | 0 -> acc
    | _ -> 
      begin
      let initial_position = powerLocation u.u_pos in
      let pow = {
      b_type = Power;
      b_id = next_available_id ();
      b_pos = initial_position;
      b_vel = power_velocity initial_position player_loc;
      b_accel = (0., 0.);
      b_radius = radius_of_bullet Power;
      b_color = Red;
      } in
      let up = AddBullet(pow.b_id, pow.b_color, pow.b_type, pow.b_pos) in
      Netgraphics.add_update up;
      createPowersRec g u player_loc (amt-1) (pow::acc)
    end
  in 
  let blue_num = cUFO_POWER_NUM - red_num in
  let redPowers = createPowersRec g u g.red_team.player.p_pos red_num [] in
  let bluePowers = createPowersRec g u g.blue_team.player.p_pos blue_num [] in
  g.powers <- redPowers@bluePowers@g.powers
  
let updatePowers (g : game) : unit =
  let fold_helper_power (acc : power list) (p : power) : power list =
    let test (x : power) = x.b_id = p.b_id in 
    let red_hit = List.exists test g.collisions.red_powers in
    let blue_hit = List.exists test g.collisions.blue_powers in
    match red_hit || blue_hit with
    | true -> Netgraphics.add_update (DeleteBullet(p.b_id)); acc
    | false -> 
    let p' = updateBulletPosition p in
    p'::acc 
  in
  g.powers <- List.fold_left fold_helper_power [] g.powers

let updateUFOs (g : game) : unit = 
  createUFO g;
  let updateUFO (u : ufo) (hits : (ufo * bullet list) list) : ufo =
    let moveUFO (u : ufo) : position =
      let (ux, uy) = u.u_pos in
      let (vx, vy) = u.u_vel in
      let position = ((ux +. vx), (uy +. vy)) in
      position
    in
    let updateHits (col : color) (u : ufo) (hits : (ufo * bullet list) list) =
      let bulletHits (b_list : bullet list) : int =
        let fold_helper (acc : int) (b : bullet) : int =
          match b.b_color = col with
          | true -> acc+1
          | false -> acc
        in
        List.fold_left fold_helper 0 b_list
      in
      let outer_fold_helper (acc : int) (tup : ufo * bullet list) : int =
        let (u', b_list) = tup in
        match u'.u_id = u.u_id with
        | true -> acc + bulletHits b_list
        | false -> acc
      in
    List.fold_left outer_fold_helper 0 hits
    in
    let return = {
    u with
    u_pos = moveUFO u;
    u_red_hits = updateHits Red u hits;
    u_blue_hits = updateHits Blue u hits;
    } in
    return
  in
  let fold_helper (acc : ufo list) (u : ufo) : ufo list =
    let u' = updateUFO u g.collisions.u_kills in
    match (u'.u_red_hits + u'.u_blue_hits) < cUFO_HITS with
    | true -> Netgraphics.add_update (MoveUFO(u'.u_id, u'.u_pos)); u'::acc
    | false -> 
      let ratio = u'.u_red_hits/(u'.u_red_hits+u'.u_blue_hits) in
      createPowers g u (cUFO_POWER_NUM*ratio);
      Netgraphics.add_update (DeleteUFO(u'.u_id));
      acc
  in
  g.ufos <- List.fold_left fold_helper [] g.ufos