open MyDefinitions
open Definitions
open Constants
open Util
open Collision

(* Code to update the teams' players
   This takes a list of updates and adds each update to the gui's list
   of updates *)
let rec sendBatchUpdates (updateList : update list) : unit =
  match updateList with
  |[] -> ()
  |h::tl -> Netgraphics.add_update h; sendBatchUpdates tl

let removeAllBullets game =
  let helper_function bullet = (DeleteBullet(bullet.b_id)) in
  let updateList = List.map helper_function game.bullets in
  sendBatchUpdates updateList;
  game.bullets <- []


let movePlayer (player : player_char) (dirs : direction * direction) =
  let getPlayerSpeed (player : player_char) : float =
    if player.p_focused
    then (float_of_int cFOCUSED_SPEED)
    else (float_of_int cUNFOCUSED_SPEED)
  in
  let (x, y) = player.p_pos in
  let speed = getPlayerSpeed player in
  let (x1, y1) = vector_of_dirs dirs speed in
  let new_x = 
    match (x +. x1 >= 0.), (x +. x1 <= (float_of_int cBOARD_WIDTH)) with
    | true, true -> x +. x1
    | true, false -> x
    | false, true -> x
    | false, false -> failwith "Just stopping compiler error."
  in
  let new_y = 
  match (y +. y1 >= 0.), (y+. y1 <= (float_of_int cBOARD_HEIGHT)) with
    | true, true -> y +. y1
    | true, false -> y
    | false, true -> y
    | false, false -> failwith "Just stopping compiler error."
  in
  let newPos = (new_x, new_y) in
  let newPlayer = {
    p_id = player.p_id;
    p_pos = newPos;
    p_focused = player.p_focused;
    p_radius = player.p_radius;
    p_color = player.p_color
  } in
  Netgraphics.add_update (MovePlayer(newPlayer.p_id, newPos));
  newPlayer

let getPlayerPosition (game : game) (col : color) : position =
  match col with
  | Red -> game.red_team.player.p_pos
  | Blue -> game.blue_team.player.p_pos

let setPlayerFocus (player : player_char) (focus : bool) : player_char =
  let newPlayer = {
    p_id = player.p_id;
    p_pos = player.p_pos;
    p_focused = focus;
    p_radius = player.p_radius;
    p_color = player.p_color
  } in
  newPlayer

let changeFocus (game : game) (col : color) (f : bool) : unit =
  match col with
  | Red -> game.red_team.player <- setPlayerFocus game.red_team.player f
  | Blue -> game.blue_team.player <- setPlayerFocus game.blue_team.player f

(* End of code to update the teams' players *)

(* Code to update other properties of the teams *)

(* Updates the number of lives a team has left by taking into account how
   many times it has been hit by a bullet in this time step. *)
let updateLives (t : team) (g : game) : int =
  let hits =
    match t.player.p_color with
    | Red -> List.length g.collisions.red_kills
    | Blue -> List.length g.collisions.blue_kills
  in
  let (b, _) = t.invincible in
  match b with
  | true -> t.lives
  | false -> 
  begin
    match hits > 0 with 
    | true -> (removeAllBullets g); max (t.lives - 1) 0
    | false -> t.lives
  end

let updateBombs (t : team) (g : game) : int =
  let hits =
    match t.player.p_color with
    | Red -> List.length g.collisions.red_kills
    | Blue -> List.length g.collisions.blue_kills
  in
  let (b, _) = t.invincible in
  if b then t.bombs else (
  match hits > 0 with
  | true -> 
    Netgraphics.add_update (SetBombs(t.player.p_color, cINITIAL_BOMBS)); 
    cINITIAL_BOMBS
  | false -> t.bombs)

let updateScore (t : team) (game : game) : int =
  let (kills, powerups, grazes) = 
    match t.player.p_color with
    | Red -> 
      ((if List.length game.collisions.blue_kills > 0 then 1 else 0), 
       (List.length game.collisions.red_powers), 
       (if List.length game.collisions.red_grazes > 0 then 1 else 0))
    | Blue -> 
      ((if List.length game.collisions.red_kills > 0 then 1 else 0), 
       (List.length game.collisions.blue_powers), 
       (if List.length game.collisions.blue_grazes > 0 then 1 else 0)) 
  in
  let killScore =  cKILL_POINTS * kills in
  let powerupScore = cPOWER_POINTS * powerups in
  let grazeScore = cGRAZE_RADIUS * grazes in
  t.score + killScore + powerupScore + grazeScore

let updatePowers (g : game) (t : team) : int =
  let (boo , _) = t.invincible in
  let numPowers =
    match t.player.p_color with
    | Red -> List.length g.collisions.red_powers
    | Blue -> List.length g.collisions.blue_powers
  in
  let temp_power = t.power + numPowers in
  match boo with
  | false ->
  begin
  let hits =
    match t.player.p_color with
    | Red -> List.length g.collisions.red_kills
    | Blue -> List.length g.collisions.blue_kills
  in
  let new_power = 
    match hits > 0 with
    | true -> temp_power / 2
    | false -> temp_power
  in
  Netgraphics.add_update (SetPower(t.player.p_color, new_power));
  new_power
  end
  | true -> temp_power

let updateCharge (t : team) (game : game) : int =
  let (col, dur) = game.remaining_bomb_duration in
  match t.player.p_color = col with
  | true ->
  begin
    match dur > 0. with
    | true -> min (t.charge+cCHARGE_RATE+t.power) cCHARGE_MAX
    | false -> t.charge
  end
  | false -> min (t.charge+cCHARGE_RATE+t.power) cCHARGE_MAX
  

(* End of code to update other properties of the teams *)

let nextMove (game : game) : unit =
   (* Changing position*)
  match game.red_team.movesQueue with
  | [] -> ()
  | h::tl -> let newPlayer = movePlayer game.red_team.player h in
  game.red_team.movesQueue <- tl;
  game.red_team.player <- newPlayer;
  match game.blue_team.movesQueue with
  | [] -> ()
  | h::tl -> let newPlayer1 = movePlayer game.blue_team.player h in
  game.blue_team.movesQueue <- tl;
  game.blue_team.player <- newPlayer1

let invincibility (t : team) (game : game) : bool * int =
  let hits =
    match t.player.p_color with
    | Red -> List.length game.collisions.red_kills
    | Blue -> List.length game.collisions.blue_kills
  in
  let (b, i) = t.invincible in
  match hits > 0 with
  | true -> (true, cINVINCIBLE_FRAMES)
  | false -> 
    match i - 1 > 0 with
    | true -> (true, i-1)
    | false -> (false, 0)

let updatePlayers (game : game) =
  nextMove game;
  game.collisions <- all_collisions game;
  game.red_team.score <- updateScore game.red_team game;
  Netgraphics.add_update (SetScore(Red, game.red_team.score));
  game.blue_team.score <- updateScore game.blue_team game;
  Netgraphics.add_update (SetScore(Blue, game.blue_team.score));
  game.red_team.bombs <- (updateBombs game.red_team game);
  game.blue_team.bombs <- (updateBombs game.blue_team game);
  game.red_team.power <- updatePowers game game.red_team;
  game.blue_team.power <- updatePowers game game.blue_team;
  game.red_team.lives <- updateLives game.red_team game;
  Netgraphics.add_update (SetLives(Red, game.red_team.lives));
  game.blue_team.lives <- updateLives game.blue_team game;
  Netgraphics.add_update (SetLives(Blue, game.blue_team.lives));
  game.red_team.invincible <- invincibility game.red_team game;
  game.blue_team.invincible <- invincibility game.blue_team game;